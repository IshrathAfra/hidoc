// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
    firebase: {
      apiKey: 'AIzaSyD2TD7AwwKe7scCvUpQLXsisiLmnYonPH8',
      authDomain: 'hidoc-5c2db.firebaseapp.com',
      databaseURL: 'https://hidoc-5c2db.firebaseio.com',
      projectId: 'hidoc-5c2db',
      storageBucket: 'hidoc-5c2db.appspot.com',
      messagingSenderId: '979080250487',
      appId: '1:979080250487:web:a6700558c484c2b28a7272',
      measurementId: 'G-ZX43D041VP'
    }
    };

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
