import { Time } from '@angular/common';

export default class doctor {
    id: string;
    name: string;
    dr: string;
    image: string;
    spcl: string;
    hosp: string;
    cit: string;
    about: string;
    ch1: boolean;
    ch2: boolean;
    ch3: boolean;
    ch4: boolean;
    ch5: boolean;
    ch6: boolean;
    ch7: boolean;
    mon: Time;
    tue: Time;
    wed: Time;
    thu: Time;
    fri: Time;
    sat: Time;
    sun: Time;

   }
