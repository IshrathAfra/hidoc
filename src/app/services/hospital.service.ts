import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import hospital  from '../models/hospital';

@Injectable({
  providedIn: 'root'
})

export class HospitalService {

  private dbPath = '/hospital';

  tutorialsRef: AngularFirestoreCollection<hospital> = null;

  constructor(private db: AngularFirestore) {
    this.tutorialsRef = db.collection(this.dbPath);
  }
  getAll(): AngularFirestoreCollection<hospital> {
    return this.tutorialsRef;
  }

  create(tutorial: hospital): any {
    return this.tutorialsRef.add({ ...tutorial });
  }
  update(id: string, data: any): Promise<void> {
    return this.tutorialsRef.doc(id).update(data);
  }

  delete(id: string): Promise<void> {
    return this.tutorialsRef.doc(id).delete();
  }
}
