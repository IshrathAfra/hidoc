import { TestBed } from '@angular/core/testing';

import { ViewdocService } from './viewdoc.service';

describe('ViewdocService', () => {
  let service: ViewdocService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ViewdocService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
