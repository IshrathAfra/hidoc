import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import viewdoc from '../models/viewdoc';
@Injectable({
  providedIn: 'root'
})
export class ViewdocService {
  private dbPath = '/viewdoc';

  tutorialsRef: AngularFirestoreCollection<viewdoc> = null;

  constructor(private db: AngularFirestore) {
    this.tutorialsRef = db.collection(this.dbPath);
  }

  getAll(): AngularFirestoreCollection<viewdoc> {
    return this.tutorialsRef;
  }
  create(tutorial: viewdoc): any {
    return this.tutorialsRef.add({ ...tutorial });
  }

  update(id: string, data: any): Promise<void> {
    return this.tutorialsRef.doc(id).update(data);
  }

  delete(id: string): Promise<void> {
    return this.tutorialsRef.doc(id).delete();
  }}

