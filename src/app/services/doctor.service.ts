
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import doctor from '../models/doctor';
@Injectable({
  providedIn: 'root'
})
export class DoctorService {
  formData: doctor;
  private dbPath = '/doctor';

  tutorialsRef: AngularFirestoreCollection<doctor> = null;

  constructor(private db: AngularFirestore) {
    this.tutorialsRef = db.collection(this.dbPath);
  }
  getAll(): AngularFirestoreCollection<doctor> {
    return this.tutorialsRef;
  }

  create(tutorial: doctor): any {
    return this.tutorialsRef.add({ ...tutorial });
  }
  update(id: string, data: any): Promise<void> {
    return this.tutorialsRef.doc(id).update(data);
  }

  delete(id: string): Promise<void> {
    return this.tutorialsRef.doc(id).delete();
  }
  // update(id: string): Promise<void> {
   // return this.tutorialsRef.doc(id).update();
  // }
}
