import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import city from '../models/city';

@Injectable({
  providedIn: 'root'
})
export class TutorialService {

  private dbPath = '/city';

  tutorialsRef: AngularFirestoreCollection<city> = null;

  constructor(private db: AngularFirestore) {
    this.tutorialsRef = db.collection(this.dbPath);
  }

  getAll(): AngularFirestoreCollection<city> {
    return this.tutorialsRef;
  }

  create(tutorial: city): any {
    return this.tutorialsRef.add({ ...tutorial });
  }
  getByid(id) {
    return this.tutorialsRef.doc(id).valueChanges();
    }

  update(id: string, data: any): Promise<void> {
    return this.tutorialsRef.doc(id).update(data);
  }

  delete(id: string): Promise<void> {
    return this.tutorialsRef.doc(id).delete();
  }
}
