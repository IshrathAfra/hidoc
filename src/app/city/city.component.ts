import { Component, OnInit } from '@angular/core';
import { TutorialService } from 'src/app/services/tutorial.service';
import city from 'src/app/models/city';
import { map } from 'rxjs/operators';
@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.scss']
})
export class CityComponent implements OnInit {
  isSelected = false;
    tutorial: city = new city();
    submitted = false;
    tutorials: any;

     constructor(private tutorialService: TutorialService) { }

     ngOnInit(): void {
        this.tutorialService.getAll().snapshotChanges().pipe(map(changes =>
                changes.map(c =>
                    ({ id: c.payload.doc.id, ...c.payload.doc.data() })
                )
            )
        ).subscribe(data => {
            this.tutorials = data;
        });
    }

    saveTutorial(): void {
        this.tutorialService.create(this.tutorial).then(() => {
            console.log('Created new item successfully!');
            this.submitted = true;
        });
    }
    delete(id: string) {
        this.tutorialService.delete(id);
    }
    newTutorial(): void {
        this.submitted = false;
        this.tutorial = new city();
    }

    setCustomerDetails(tutorials: city) {
      this.isSelected = !this.isSelected;
      if (this.isSelected) {
        this.tutorials = city;
      } else {
        this.tutorials = undefined;
      }
    }
  }
