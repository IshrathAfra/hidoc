import { Component, OnInit } from '@angular/core';

import { SpecialService } from 'src/app/services/special.service';
import special from 'src/app/models/special';


import { finalize, map } from 'rxjs/operators';
import {AngularFireStorage} from '@angular/fire/storage';
import firebase from 'firebase';
@Component({
  selector: 'app-special',
  templateUrl: './special.component.html',
  styleUrls: ['./special.component.scss']
})
export class SpecialComponent implements OnInit {
  private basePath = '/images';
  file: File;
  url = '';

  path: string;
  special: special = new special();
  submitted = false;
  tutorials: any;

  ref: any;
  task: any;
  uploadProgress: any;
  downloadURL: any;
  fb: any;
  urlImage: any;

  constructor(
    private specialService: SpecialService ,
    private af: AngularFireStorage,
    private storage: AngularFireStorage,
    ) { }
  handleFiles(event) {
    this.file = event.target.files[0];
  }

  // upload($event) {
  //   this.path = $event.target.files[0];
  // }


  // uploadImage() {
  //   console.log(this.path);
  //   this.af.upload('/files' + Math.random() + this.path, this.path);
  // }

  upload(event) {
    if (event.target.files) {
      const reader: FileReader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event: any) => {
        this.url = event.target.result;
        this.file = event.target.files[0];
      };
    }
    const n = Date.now();
    const file = event.target.files[0];
    const filePath = `special/${n}`;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(`special/${n}`, file);
    task
      .snapshotChanges()
      .pipe(
        finalize(() => {

          this.downloadURL = fileRef.getDownloadURL();
          this.downloadURL.subscribe(url => {
            if (url) {
              this.fb = url;

              console.log('url', url);
              console.log('fb', this.fb);

            }

          });
        })
      )
      .subscribe(url => {
        if (url) {
          this.urlImage = url;
          console.log('urlll', url);
              console.log('urlimag', this.urlImage);
        }
      });

  }

  ngOnInit(): void {
    this.specialService.getAll().snapshotChanges().pipe(map(changes =>
      changes.map(c =>
          ({ id: c.payload.doc.id, ...c.payload.doc.data() })
      )
  )
).subscribe(data => {
  this.tutorials = data;
});
}

  saveTutorial(): void {
    this.special.image = this.fb;
      this.specialService.create(this.special).then(() => {
          console.log('Created new item successfully!');
          this.submitted = true;
      });
  }
  delete(id: string) {
    this.specialService.delete(id);
  }

  newTutorial(): void {
      this.submitted = false;
      this.special = new special();
  }


}
