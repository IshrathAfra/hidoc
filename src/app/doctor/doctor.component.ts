import { Component, Input, OnInit } from '@angular/core';
import doctor from 'src/app/models/doctor';
import { finalize, map } from 'rxjs/operators';
import { TutorialService } from 'src/app/services/tutorial.service';
import special from '../models/special';
import hospital from 'src/app/models/hospital';
import city from 'src/app/models/city';
import { SpecialService } from 'src/app/services/special.service';
import {  HospitalService} from 'src/app/services/hospital.service';
import { DoctorService } from 'src/app/services/doctor.service';
import firebase from 'firebase';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
@Component({

  selector: 'app-doctor',
  templateUrl: './doctor.component.html',
  styleUrls: ['./doctor.component.scss'],
})
export class DoctorComponent implements OnInit {
  private basePath = '/images';
  file: File;
  url = '';
  path: string;
   isSelected = false;

   doctornew: any;

  doctors: any;
  doctor: doctor = new doctor();

  specials: any;
  special: special = new special();

  hospitalnew: any;
  hospital: hospital = new hospital();

  tutorials: any;
  turorials: city = new city();

  submitted = false;
  ref: any;
  task: any;
  uploadProgress: any;
  downloadURL: any;
  fb: any;
  urlImage: any;

  constructor(private af: AngularFireStorage,
    private doctorService: DoctorService, private tutorialService: DoctorService, private cityService: TutorialService,
     private specialServices: SpecialService, private hospitalService: HospitalService, private storage: AngularFireStorage, ) { }

     handleFiles(event) {
      this.file = event.target.files[0];
    }

    upload(event) {
      if (event.target.files) {
        const reader: FileReader = new FileReader();
        reader.readAsDataURL(event.target.files[0]);
        reader.onload = (event: any) => {
          this.url = event.target.result;
          this.file = event.target.files[0];
        };
      }
      const n = Date.now();
      const file = event.target.files[0];
      const filePath = `doctor/${n}`;
      const fileRef = this.storage.ref(filePath);
      const task = this.storage.upload(`doctor/${n}`, file);
      task
        .snapshotChanges()
        .pipe(
          finalize(() => {

            this.downloadURL = fileRef.getDownloadURL();
            this.downloadURL.subscribe(url => {
              if (url) {
                this.fb = url;

                console.log('url', url);
                console.log('fb', this.fb);

              }

            });
          })
        )
        .subscribe(url => {
          if (url) {
            this.urlImage = url;
            console.log('urlll', url);
                console.log('urlimag', this.urlImage);
        }
      });

    }

     // method to retrieve download url
  private async getUrl(snap: firebase.storage.UploadTaskSnapshot) {
    const url = await snap.ref.getDownloadURL();
    this.url = url;  // store the URL
    console.log(this.url);
  }

    ngOnInit() {
        this.doctorService.getAll().snapshotChanges().pipe(map(changes =>
      changes.map(c =>
          ({ id: c.payload.doc.id, ...c.payload.doc.data() })
      )
  )
).subscribe(data => {
  this.doctornew = data;
});





    this.cityService.getAll().snapshotChanges().pipe(map(changes =>
            changes.map(c =>
                ({ id: c.payload.doc.id, ...c.payload.doc.data() })
            )
        )
    ).subscribe(data => {
        this.tutorials = data;
    });


    this.specialServices.getAll().snapshotChanges().pipe(map(changes =>
      changes.map(c =>
          ({ id: c.payload.doc.id, ...c.payload.doc.data() })
      )
     )
      ).subscribe(data => {
        this.specials = data;
      });


this.hospitalService.getAll().snapshotChanges().pipe(map(changes =>
  changes.map(c =>
      ({ id: c.payload.doc.id, ...c.payload.doc.data() })
  )
)
).subscribe(data => {
this.hospitalnew = data;
});
  }


saveTutorial(): void {
  this.doctor.image = this.fb;
    this.tutorialService.create(this.doctor).then(() => {
        console.log('Created new item successfully!');
        this.submitted = true;
    }); }

newTutorial(): void {
    this.submitted = false;
    this.doctor = new doctor();
}


delete(id: string) {
  this.doctorService.delete(id);
}
// update(id: string){
 // this.doctorService.update(id);
// }
  setCustomerDetails(doctors: doctor) {
    this.isSelected = !this.isSelected;
    if (this.isSelected) {
      this.doctors = doctors;
    } else {
      this.doctor = undefined;
    }
  }

}
