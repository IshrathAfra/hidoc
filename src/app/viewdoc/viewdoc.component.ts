import { Component, OnInit } from '@angular/core';
import {  DoctorService} from 'src/app/services/doctor.service';
import doctor from 'src/app/models/doctor';
import { map } from 'rxjs/operators';
@Component({
  selector: 'app-viewdoc',
  templateUrl: './viewdoc.component.html',
  styleUrls: ['./viewdoc.component.scss']
})

export class ViewdocComponent implements OnInit {

  doctor: doctor = new doctor();
  doctornew: any;
  submitted = false;
   constructor(private doctorService: DoctorService) { }

  ngOnInit(): void {

    this.doctorService.getAll().snapshotChanges().pipe(map(changes =>
      changes.map(c =>
          ({ id: c.payload.doc.id, ...c.payload.doc.data() })
      )
  )

).subscribe(data => {
  this.doctornew = data;
});


  }
  saveTutorial(): void {
    this.doctorService.create(this.doctor).then(() => {
        console.log('Created new item successfully!');
        this.submitted = true;
    });
  }
delete(id: string) {
  this.doctorService.delete(id);
}
onEdit(doc: doctor) {
  this.doctorService.formData = Object.assign({}, doc);
}
// update(id: string) {
 // this.doctorService.update(id);
// }

}
