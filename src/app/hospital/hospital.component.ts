import { Component, OnInit } from '@angular/core';
import {  HospitalService} from 'src/app/services/hospital.service';
import hospital from 'src/app/models/hospital';
import { map } from 'rxjs/operators';
import city from 'src/app/models/city';
import { TutorialService } from 'src/app/services/tutorial.service';

@Component({
  selector: 'app-hospital',
  templateUrl: './hospital.component.html',
  styleUrls: ['./hospital.component.scss']
})
export class HospitalComponent implements OnInit {
  hospital: hospital = new hospital();
  hospitalnew: any;
  submitted = false;
  tutorials: any;
  turorials: city = new city();
   constructor(private hospitalService: HospitalService, private cityService: TutorialService) { }

  ngOnInit(): void {
    this.cityService.getAll().snapshotChanges().pipe(map(changes =>
        changes.map(c =>
            ({ id: c.payload.doc.id, ...c.payload.doc.data() })
        )
    )
).subscribe(data => {
    this.tutorials = data;
});
this.hospitalService.getAll().snapshotChanges().pipe(map(changes =>
    changes.map(c =>
        ({ id: c.payload.doc.id, ...c.payload.doc.data() })
    )
)
).subscribe(data => {
this.hospitalnew = data;
});


  }
  saveTutorial(): void {
    this.hospitalService.create(this.hospital).then(() => {
        console.log('Created new item successfully!');
        this.submitted = true;
    });
}

delete(id: string) {
    this.hospitalService.delete(id);
}

newTutorial(): void {
    this.submitted = false;
    this.hospital = new hospital();
}

}
