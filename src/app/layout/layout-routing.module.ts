import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LayoutComponent } from './layout.component';
import { Screen2Component } from './screen2/screen2.component';

import { HospitalComponent } from '../hospital/hospital.component';
import { DoctorComponent } from '../doctor/doctor.component';
import { CityComponent } from '../city/city.component';
import { PolicyListComponent } from '../policy-list/policy-list.component';
import { SpecialComponent } from '../special/special.component';
import { ViewdocComponent } from '../viewdoc/viewdoc.component';
import { HospitalListComponent } from '../hospital-list/hospital-list.component';
import { SpecialListComponent } from '../special-list/special-list.component';
import { CityListComponent } from '../city-list/city-list.component';
import { CityEditComponent } from '../city-edit/city-edit.component';
import { SpecialEditComponent } from '../special-edit/special-edit.component';
import { HospEditComponent } from '../hosp-edit/hosp-edit.component';
import { DocEditComponent } from '../doc-edit/doc-edit.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {
                path: '',
                redirectTo: 'dashboard'
            },

            {
                path: 'dashboard',
                loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
            },

            {
                path: 'screen2',
                component: Screen2Component
            },

            {
                path: 'Hospital',
                component: HospitalComponent
            },
            {
                path: 'Hospital-List',
                component: HospitalListComponent
            },
            {
                path: 'hosp-edit/:hospital.id/:hospital.name/:hospital.add/:hospital.phn/:hospital.ci',
                component: HospEditComponent

            },


            {
                path: 'special',
                component: SpecialComponent

            },
            {
                path: 'special-List',
                component: SpecialListComponent

            },
            {
                path: 'special-edit/:special.id/:special.name/special.image',
                component: SpecialEditComponent
            },
            {
                path: 'doctor',
                component: DoctorComponent
            },
            {
                path: 'doc-edit/:doctor.id',
                component: DocEditComponent
            },


            {
                path: 'city',

                component: CityComponent,

            },
            {
                path: 'city-List',
                component: CityListComponent
            },
            {
                path: 'city-edit/:tutorial.id/:tutorial.name',
                component: CityEditComponent
            },
            {
                path: 'policy-list',
                component: PolicyListComponent
            },
            {
                path: 'viewdoc',
                component: ViewdocComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class LayoutRoutingModule {}
