import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Policy } from '../../shared/modules/policy.model';
@Injectable({
  providedIn: 'root'
})
export class PolicyService {

  constructor(private firestore: AngularFirestore) { }

getPolicies() {
  return this.firestore.collection('city').snapshotChanges();
}
createPolicy(policy: Policy){
  return this.firestore.collection('city').add(policy);
}
updatePolicy(policy: Policy){
  delete policy.id;
  this.firestore.doc('city/' + policy.id).update(policy);
}
deletePolicy(policyId: string){
  this.firestore.doc('city/' + policyId).delete();
}
}
