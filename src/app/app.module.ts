import { LayoutModule } from '@angular/cdk/layout';
import { OverlayModule } from '@angular/cdk/overlay';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';

import {AngularFireModule} from '@angular/fire';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {environment} from '../environments/environment';


import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PolicyListComponent } from './policy-list/policy-list.component';
import { HospitalComponent } from './hospital/hospital.component';
import { DoctorComponent } from './doctor/doctor.component';
 import { CityComponent } from './city/city.component';
import { SpecialComponent } from './special/special.component';
import { ViewdocComponent } from './viewdoc/viewdoc.component';
import { HospitalListComponent } from './hospital-list/hospital-list.component';
import { CityListComponent } from './city-list/city-list.component';
import { SpecialListComponent } from './special-list/special-list.component';
import { CityEditComponent } from './city-edit/city-edit.component';

import { SpecialEditComponent } from './special-edit/special-edit.component';
import { HospEditComponent } from './hosp-edit/hosp-edit.component';
import { DocEditComponent } from './doc-edit/doc-edit.component';

// AoT requires an exported function for factories
export const createTranslateLoader = (http: HttpClient) => {
    /* for development
    return new TranslateHttpLoader(
        http,
        '/start-javascript/sb-admin-material/master/dist/assets/i18n/',
        '.json'
    );*/
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
};

@NgModule({
    declarations: [AppComponent, PolicyListComponent, HospitalComponent,
        DoctorComponent, CityEditComponent, CityComponent, SpecialComponent,
        ViewdocComponent, HospitalListComponent, CityListComponent, SpecialListComponent, CityEditComponent,  SpecialEditComponent, HospEditComponent, DocEditComponent],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        LayoutModule,
        OverlayModule,
        FormsModule,
        HttpClientModule,
        AngularFirestoreModule,
        AngularFireDatabaseModule,
        AngularFireModule.initializeApp(environment.firebase),
        AppRoutingModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        })
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}
