import { Component, OnInit } from '@angular/core';
import { PolicyService } from 'src/app/shared/services/policy.service';
import { Policy } from 'src/app/shared/modules/policy.model';
@Component({
  selector: 'app-policy-list',
  templateUrl: './policy-list.component.html',
  styleUrls: ['./policy-list.component.scss']
})
export class PolicyListComponent implements OnInit {
  policies: Policy[];
  constructor(private policyService: PolicyService) { }

  ngOnInit(): void {
    this.policyService.getPolicies().subscribe(data => {
      this.policies = data.map(e => {
        return {
          id: e.payload.doc.id,
          ...e.payload.doc.data() as object
        } as Policy;
      })
    });
  }
  create(policy: Policy){
    this.policyService.createPolicy(policy);
}
update(policy: Policy) {
  this.policyService.updatePolicy(policy);
}

}


